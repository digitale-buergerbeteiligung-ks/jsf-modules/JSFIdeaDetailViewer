package servicetrackers;
import com.liferay.portal.kernel.service.UserLocalService;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class UserLocalServiceTracker extends ServiceTracker<UserLocalService, UserLocalService> {

    public UserLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, UserLocalService.class, null);
    }
}
