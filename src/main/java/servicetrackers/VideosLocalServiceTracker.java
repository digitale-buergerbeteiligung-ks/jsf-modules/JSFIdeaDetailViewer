package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import ideaService.service.VideosLocalService;

public class VideosLocalServiceTracker extends ServiceTracker<VideosLocalService, VideosLocalService> {

    public VideosLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, VideosLocalService.class, null);
    }
}
